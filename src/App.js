import React from 'react';
import './App.css';
import Title from './components/TitleComp';
import UserComp from './components/UserComp';

class App extends React.Component {

  render(){
  return (
    <div className="App">
    <Title titleText="This is title"/>
    <UserComp/>
    </div>
  );
  }
}

export default App;
