import React from 'react'

function Phone(props) {
    return (
        <div className="phone">
            {props.phone}
        </div>
    )
}

export default Phone
