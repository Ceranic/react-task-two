import React from 'react'


function Tags(props) {
    return (
        <div>
            {props.tags.map((text,index) => (
                <div className="tag" key={index}>
                    {text}
                </div>
            ))}
        </div>
    )
}

export default Tags
