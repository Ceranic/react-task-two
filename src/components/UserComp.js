import React,{useState} from 'react'
import FullName from './FullNameComp'
import Image from './ImageComp'
import About from './AboutComp'
import Email from './EmailComp'
import Phone from './PhoneComp'
import Tags from './TagsComp'
import Registred from './RegisteredComp'
import UserData from '../data/users.js'
import 'bootstrap/dist/css/bootstrap.min.css';


function UserComp(props) {
    const [currentUser, setCurrentUser] = useState(0);
    const prevUser = () => {
            if (currentUser > 0 ) setCurrentUser(currentUser - 1);
    }
    const nextUser = () => {
            if (currentUser < UserData.length - 1 ) setCurrentUser(currentUser + 1);
    }
    return (
        <div className="test">
            <div>
                <button onClick={prevUser} className="btn btn-outline-primary">Previous</button>
                <button onClick={nextUser} className="btn btn-outline-primary">Next</button>
            </div>
            <FullName name={UserData[currentUser].name}/>
            <Image imgSrc={UserData[currentUser].picture} alt="Picture"/>
            <About about={UserData[currentUser].about}/>
            <Email email={UserData[currentUser].email}/>
            <Phone phone={UserData[currentUser].phone}/>
            <Tags tags={UserData[currentUser].tags}/>
            <Registred isRegistered={UserData[currentUser].isRegistered}/>
        </div>
    )
}

export default UserComp
