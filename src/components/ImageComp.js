import React from 'react'

function Image(props) {
    return (
        <img src={props.imgSrc} alt={props.alt} className="img">
            
        </img>
    )
}

export default Image
