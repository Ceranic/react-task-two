import React from 'react'

function FullName(props) {

    return (
        <div className="fullName">
            {props.name.first} {props.name.last}
        </div>
    )
}


export default FullName
